/*global browser*/
function init(prefs) {

  let byID = function(id) {
    return document.getElementById(id);
  };

  let switchClassInIDs = function(ids, classname, index) {
    for (let i in ids) {
      if (i == index) byID(ids[i]).classList.add(classname);
      else byID(ids[i]).classList.remove(classname);
    }
  };

  let switchClasses = function(id, classnames, index) {
    for (let i in classnames) {
      if (i == index) byID(id).classList.add(classnames[i]);
      else byID(id).classList.remove(classnames[i]);
    }
  };

  let setLocalStorage = function(key, value) {
    let v = {};
    v[key] = value;
    prefs[key] = value;
    browser.storage.local.set(v);
  };

  let changePreset = function(presetname) {
    let names = ["red", "green", "blue"];
    let index = names.indexOf(presetname);
    if (index == -1) return;
    switchClasses("inner-pane", classesPaneBackground, index);
    switchClassInIDs(idsPresetButtons, "selected", index);
    // load preset values
    for (let slider of byID("main-pane").getElementsByClassName("slider-div")) {
      slider.loadprefvalue(presetname + "-");
    }
  };

  let updateGraph = function(prefix, graph) {
    if ((new Date()).getTime() - window["_yass_lastupdategraph_" + graph.id] < 50) return;
    window["_yass_lastupdategraph_" + graph.id] = (new Date()).getTime();

    let step = prefs[prefix + "step"];
    let bdump = prefs[prefix + "pre-smooth"] / 890 + 0.01;
    let edump = (900 - prefs[prefix + "post-smooth"]) / 1000;
    let c = graph.getContext("2d");
    c.clearRect(0, 0, 300, 100);

    // draw guide line
    c.fillStyle = "#aaa";
    for (let t = 1; t <= 6; t++) {
      c.fillRect(t * 50 - 1, t * 15, 1, 100 - t * 15);
    }

    c.fillStyle = "#bbb";
    c.strokeStyle = "#000";
    c.beginPath();
    c.moveTo(500, 500);
    c.lineTo(0, 500);

    var dest = (60.0 / 300.0) * step + 40.0;
    var d = 0;
    var bdumpbase = bdump * 110.0;
    var tipw = 5;
    for (var t = 0; t <= 60; t++) {
      if (d + 0.3 > dest) {
        break;
      }
      var bd = Math.min(Math.max(((t + 1) / (bdumpbase)), 0.05), 1.0);
      d += (dest - d) * edump * bd;
      c.lineTo(t * tipw, 100 - (dest - d));
    }
    c.stroke();
    c.fill();
  };

  // i18n text replace
  for (let e of document.querySelectorAll("[i18n]")) {
    e.innerText = browser.i18n.getMessage(e.getAttribute("i18n"));
  }
  for (let e of document.querySelectorAll("[title]")) {
    let title = e.getAttribute("title");
    if (title.indexOf("__") == 0) { e.title = browser.i18n.getMessage(title.substring(2)); }
  }

  // pane buttons
  let idsPanes = ["main-pane", "options-pane", "blacklist-pane", "credits-pane"];
  let idsPaneButtons = ["main-pane-button", "options-pane-button", "blacklist-pane-button", "credits-pane-button"];
  let idsDeviceButtons = ["wheel-button", "keyboard-button"];
  let idsDevicePanes = ["wheel-sliders", "keyboard-sliders"];

  byID("main-pane-button").onclick = function() {
    switchClassInIDs(idsPanes, "visible", 0);
    switchClassInIDs(idsPaneButtons, "selected", 0);
  };
  byID("options-pane-button").onclick = function() {
    switchClassInIDs(idsPanes, "visible", 1);
    switchClassInIDs(idsPaneButtons, "selected", 1);
  };
  byID("blacklist-pane-button").onclick = function() {
    switchClassInIDs(idsPanes, "visible", 2);
    switchClassInIDs(idsPaneButtons, "selected", 2);
  };
  byID("credits-pane-button").onclick = function() {
    switchClassInIDs(idsPanes, "visible", 3);
    switchClassInIDs(idsPaneButtons, "selected", 3);
  };

  byID("main-pane-button").onclick();

  // preset buttons
  let idsPresetButtons = ["red-preset-button", "green-preset-button", "blue-preset-button"];
  let classesPaneBackground = ["red-bg", "green-bg", "blue-bg"];

  // change active pane by storage change event
  browser.storage.onChanged.addListener(function(changes, area) {
    if (area != "local") return;
    if (changes["selected-preset"] === undefined) return;
    changePreset(changes["selected-preset"].newValue);
    // prefs["selected-preset"] <- referred on nowhere so no need to set
  });

  // button -[message]-> background -> set storage -[change event]-> callback -> change interface
  byID("red-preset-button").onclick = function() {
    browser.runtime.sendMessage({type: "change-preset", value: "red"});
  };
  byID("green-preset-button").onclick = function() {
    browser.runtime.sendMessage({type: "change-preset", value: "green"});
  };
  byID("blue-preset-button").onclick = function() {
    browser.runtime.sendMessage({type: "change-preset", value: "blue"});
  };

  byID("wheel-button").onclick = function() {
    switchClassInIDs(idsDeviceButtons, "selected", 0);
    switchClassInIDs(idsDevicePanes, "visible", 0);
  };
  byID("keyboard-button").onclick = function() {
    switchClassInIDs(idsDeviceButtons, "selected", 1);
    switchClassInIDs(idsDevicePanes, "visible", 1);
  };

  byID("wheel-button").onclick();

  // sliders
  for (let slider of document.getElementsByClassName("slider-div")) {
    let input = slider.querySelector("input");
    let num = slider.querySelector("div");
    let forgraph = slider.getAttribute("forgraph");
    if (forgraph) forgraph = byID(forgraph);

    slider.prefkey = slider.id;

    slider.prefValue = function(value) { return value; };
    slider.inputValue = function(value) { return value; };

    input.oninput = function() {
      num.innerText = slider.displayValue(input.value);
      prefs[slider.prefkey] = slider.prefValue(input.value);
      if (forgraph) updateGraph(slider.prefgroupprefix, forgraph);
    };

    input.onchange = function() {
      setLocalStorage(slider.prefkey, prefs[slider.prefkey]);
      if (forgraph) updateGraph(slider.prefgroupprefix, forgraph);
    };

    slider.displayValue = function(value) { return value; };

    slider.loadprefvalue = function(prefix = "") {
      slider.prefkey = prefix + slider.id;
      let prefgroupprefix = slider.prefkey.match("[a-z]+\\-[a-z]+\\-");
      if (prefgroupprefix !== null) slider.prefgroupprefix = prefgroupprefix[0];
      input.value = slider.inputValue(prefs[slider.prefkey]);
      input.oninput();
    };
  }

  let offcaption = browser.i18n.getMessage("edgemenu_item0");
  byID("bouncy-edge").displayValue = function(v) { return (v <= 0) ? offcaption : v; };
  byID("bouncy-edge").loadprefvalue();
  byID("accel-travel").displayValue = function(v) { return (v <= 0) ? offcaption : ((v < 1) ? v : (1 / (2 - v)).toFixed(2)); };
  byID("accel-travel").prefValue = function(v) { return (v < 1) ? v : (1 / (2 - v)); };
  byID("accel-travel").inputValue = function(v) { return (v < 1) ? v : (2 - (1 / v)); };
  let smoothPrefValue = function(v) { return v * 8.9; };
  let smoothInputValue = function(v) { return (v * (10.0 / 89.0)).toFixed(2); };
  byID("w-pre-smooth").prefValue = smoothPrefValue;
  byID("w-pre-smooth").inputValue = smoothInputValue;
  byID("k-pre-smooth").prefValue = smoothPrefValue;
  byID("k-pre-smooth").inputValue = smoothInputValue;
  byID("w-post-smooth").prefValue = smoothPrefValue;
  byID("w-post-smooth").inputValue = smoothInputValue;
  byID("k-post-smooth").prefValue = smoothPrefValue;
  byID("k-post-smooth").inputValue = smoothInputValue;
  byID("k-accel").prefValue = function(v) { return v * 100; };
  byID("k-accel").inputValue = function(v) { return v / 100; };

  changePreset(prefs["selected-preset"]);

  // all checkboxes
  for (let checkbox of document.querySelectorAll("input[type=checkbox]")) {
    checkbox.onchange = function() {
      setLocalStorage(checkbox.id, checkbox.checked);
    };
    checkbox.checked = prefs[checkbox.id];
  }

  // blacklist
  let blacklist = byID("blacklist");
  blacklist.onchange = function() {
    let list = blacklist.value.split(/\n/).reduce(function(prev, elm) {
      if (typeof(elm) == "string") {
        let s = elm.replace(/^\s+|\s+$/g, "");
        if (s.length > 0) {
          prev.push(s.replace(/\./g, "\\.").replace(/\+/g, "\\+").replace(/\?/g, "\\?").replace(/\*/g, ".*") + "$");
        }
      }
      return prev;
    }, []);
    setLocalStorage("blacklist", list);
  };
  blacklist.value = prefs["blacklist"].join("\n").replace(/\\/g, "").replace(/\.\*/g, "*").replace(/\$/g, "");
}

document.addEventListener("DOMContentLoaded", function(e) {
  document.title = browser.i18n.getMessage("prefwindow_title");
  browser.storage.local.get(null).then(init);
});
