/*global gContextMenu,yass*/
window.addEventListener("load", function() {

  document.getElementById("contentAreaContextMenu").addEventListener("popupshowing", function() {
      if (!yass.m_prefservice.getBoolPref("extensions.yass.showcontextmenu")) return;
      var hideall = false;
      if (gContextMenu.onImage || gContextMenu.onTextInput || gContextMenu.onMathML || gContextMenu.onLink || gContextMenu.onMailtoLink || gContextMenu.isContentSelected) hideall = true;
      var presetid = yass.m_prefservice.getCharPref("extensions.yass.selectedpreset");
      var list = ["red", "green", "blue"];
      for (var i in list) {
        document.getElementById("yass-changepreset-" + list[i]).hidden = (hideall || list[i].indexOf(presetid) >= 0);
      }
      document.getElementById("yass-changepreset-sep").hidden = hideall;
    }, false);

}, false);
