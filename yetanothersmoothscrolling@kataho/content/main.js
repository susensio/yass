/*global Components,yassev,gBrowser*/
var yass = {

  m_pref: {},
  m_prefservice: Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch),
  m_consoleservice: Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService),
  m_timer: Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer),
  m_gmm: Components.classes["@mozilla.org/globalmessagemanager;1"].getService(Components.interfaces.nsIMessageListenerManager),

  oneshot: function(func, delay) {
    this.m_timer.initWithCallback(func, delay, 0);
  },
  range: function(t, min, max) {
    return (t < min) ? min : ((t > max) ? max : t);
  },
  dump: function(s) {
    if (this.m_pref.dolog) this.m_consoleservice.logStringMessage(s);
  },

  refreshPreferences: function() {
    // broadcast message
    yass.refreshPreferencesLocal();
    yass.m_gmm.broadcastAsyncMessage("YASmoothScrolling@kataho:refreshPreferences");
  },

  refreshPreferencesLocal: function() {
    this.m_pref.dolog = false;
    try {
      this.m_pref.dolog = this.m_prefservice.getBoolPref("extensions.yass.dolog");
    }
    catch (e) {}

    yassev.update_indicator();

  },

  change_preset: function(val) {
    this.m_prefservice.setCharPref("extensions.yass.selectedpreset", val);
    this.refreshPreferences(null);
  },

  toggle_enable: function(sw) {
    if (sw == "")
      sw = (this.m_prefservice.getCharPref("extensions.yass.enabled") == "true") ? "false" : "true";
    this.m_prefservice.setCharPref("extensions.yass.enabled", sw);
    this.refreshPreferences(null);
  }
};

// runs per window (top level window)
// with just function(){..} , sometimes called before overlay load finish and getElementById fails so...
addEventListener("load", function() {

  window.messageManager.addMessageListener("YASmoothScrolling@kataho:set_tickwheel", function(msg) {
    yass.m_prefservice.setIntPref("extensions.yass.tickwheel", msg.data.tickwheel);
  });

  gBrowser.tabContainer.addEventListener("TabSelect", function(ev) {
    window.messageManager.broadcastAsyncMessage("YASmoothScrolling@kataho:reset_design_mode_check_target");
  }, false);

  window.messageManager.loadFrameScript("chrome://yass/content/frame.js", true);

  var ps = yass.m_prefservice;
  // convert old preferences into new preference system
  do { // eslint-disable-line no-constant-condition
    var prefs = [];
    var v = ps.getCharPref("extensions.yass.prefversion") || "0";
    if (parseInt(v) >= 2) break;
    prefs.push(parseInt(ps.getCharPref("extensions.yass.wheelstep")));
    prefs.push(parseInt(ps.getCharPref("extensions.yass.wheeldumping")));
    prefs.push(parseInt(ps.getCharPref("extensions.yass.wheelbdumping")));
    prefs.push(parseInt(ps.getCharPref("extensions.yass.wheelaccel")));
    prefs.push(parseInt(ps.getCharPref("extensions.yass.kbdstep")));
    prefs.push(parseInt(ps.getCharPref("extensions.yass.kbddumping")));
    prefs.push(parseInt(ps.getCharPref("extensions.yass.kbdbdumping")));
    prefs.push(parseInt(ps.getCharPref("extensions.yass.kbdaccel")));
    ps.setCharPref("extensions.yass.preset.red", prefs.toString());
    ps.setCharPref("extensions.yass.prefversion", "2");
    yass.dump("yass: old parameters have been converted.\n");
  } while (0);

  // pref version 4 / get all wheel input as pixelscroll
  do { // eslint-disable-line no-constant-condition
    var v = ps.getCharPref("extensions.yass.prefversion") || "0"; // eslint-disable-line no-redeclare
    if (parseInt(v) >= 4) break;

    if (ps.getBoolPref("extensions.yass.usepixelscroll")) {
      var f = (ps.getCharPref("extensions.yass.pixelscrollfactor") || "1.0") - 0;
      var selpreset = ps.getCharPref("extensions.yass.selectedpreset") || "red";
      var strpreset = ps.getCharPref("extensions.yass.preset." + selpreset);
      var arpreset = strpreset.split(",");
      var i = yass.range(28.0 * f, 1.0, 300.0); // 28 = 1 tick of wheel <- wrong!!
      arpreset[0] = "" + parseInt("" + i);
      ps.setCharPref("extensions.yass.preset." + selpreset, arpreset.join(","));
    }
    ps.setIntPref("extensions.yass.smoothing", parseInt(v));

    ps.setCharPref("extensions.yass.prefversion", "4");
    yass.dump("yass: old parameters have been converted to v4.\n");
  } while (0);

  do { // eslint-disable-line no-constant-condition
    var v = parseInt(ps.getCharPref("extensions.yass.prefversion") || "0"); // eslint-disable-line no-redeclare
    if (v >= 5) break;
    ps.setIntPref("extensions.yass.tickwheel", 84);
    ps.setCharPref("extensions.yass.prefversion", "5");
    yass.dump("yass: old parameters have been converted to v5.\n");
  } while (0);

  yass.oneshot(function() {
    yass.refreshPreferencesLocal();
  }, 10);
});

window.addEventListener("unload", function() {
  yassev.onunload();
});
