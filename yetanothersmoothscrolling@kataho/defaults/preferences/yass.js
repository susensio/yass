pref("extensions.yetanothersmoothscrolling@kataho.description","chrome://yass/locale/description.properties");
pref("extensions.yass.usekbd", true);
pref("extensions.yass.selectedpreset", "green");
pref("extensions.yass.preset.red", "70,575,20,70,100,588,14,100");
pref("extensions.yass.preset.green", "60,707,130,163,86,600,124,105");
pref("extensions.yass.preset.blue", "30,792,187,416,70,823,244,110");
pref("extensions.yass.enabled","true");
pref("extensions.yass.blacklist","");

pref("extensions.yass.showcontextmenu",true);
pref("extensions.yass.showstatusbar",true);

pref("extensions.yass.prefversion","5");
pref("extensions.yass.edgetype",60);
pref("extensions.yass.useflickemulation",false);

pref("extensions.yass.usepagejump",false);
pref("extensions.yass.usewholejump",false);
pref("extensions.yass.pagemargin",40);

pref("extensions.yass.smoothing",3);
pref("extensions.yass.tickwheel",84);
// old prefs 
pref("extensions.yass.usepixelscroll",true);
pref("extensions.yass.pixelscrollfactor", "1.00011");
pref("extensions.yass.wheelstep", "70");
pref("extensions.yass.wheeldumping", "490");
pref("extensions.yass.wheelbdumping", "1");
pref("extensions.yass.wheelaccel", "70");
pref("extensions.yass.kbdstep", "100");
pref("extensions.yass.kbddumping", "400");
pref("extensions.yass.kbdbdumping", "0");
pref("extensions.yass.kbdaccel", "100");
